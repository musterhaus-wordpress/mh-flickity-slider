<?php

/**
 * Plugin Name: Musterhaus - Flickity Slider
 * Plugin URI: https://bitbucket.org/musterhaus-wordpress/mh-flickity-slider
 * Description:
 * Version: 0.1.0
 * Author: Sebastian Hübner <huebner@musterhaus.net>
 * License: LGPL-2.0
 *
 * @version 0.1.0
 * @author  Sebastian Hübner <huebner@musterhaus.net>
 * @package Musterhaus\FlickySliderWP
 */

spl_autoload_register(function ($class) {
    $prefix = 'Musterhaus\\FlickySliderWP\\';
    $base_dir = realpath(dirname(__FILE__)) . '/src/';
    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        return;
    }
    $relative_class = substr($class, $len);
    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
    if (file_exists($file)) {
        require $file;
    }
});

if (!defined('MH_FLICKY_SLIDER_DIR')) define('MH_FLICKY_SLIDER_DIR', realpath(__DIR__));
if (!defined('MH_FLICKY_SLIDER_URL')) define('MH_FLICKY_SLIDER_URL', plugins_url('', __FILE__));

$flickySlider = new \Musterhaus\FlickySliderWP\FlickySlider();