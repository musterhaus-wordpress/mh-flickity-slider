/**
 *
 */
var flickySlider = (function ($) {

    $('.flicky-slider').each(function () {
        var $carousel = $(this);

        $carousel.removeClass('is-hidden');
        $carousel[0].offsetHeight;

        var autoPlay = false;
        if ($carousel.data('auto-play') === 1) {
            autoPlay = true;
        }

        var groupCells = null;
        var groupCellsSetting = $carousel.data('group-cells');
        if (groupCellsSetting === true) {
            groupCells = true;
        }
        else if (parseInt(groupCellsSetting) > 0) {
            groupCells = parseInt(groupCellsSetting);
        }

        var cellAlign = ($carousel.data('cell-align') === undefined) ? 'left' : $carousel.data('cell-align');
        var showButtons = ($carousel.data('buttons') === undefined) ? true : $carousel.data('buttons');
        var showDots = ($carousel.data('dots') === undefined) ? true : $carousel.data('dots');
        var wrapAround = ($carousel.data('wrap-around') === undefined) ? true : $carousel.data('wrap-around');
        var draggable = ($carousel.data('draggable') === undefined) ? true : $carousel.data('draggable');

        var flickityDefault = 'enabled',
            flickityOptions = {
                // options
                imagesLoaded: true,
                contain: true,
                cellAlign: cellAlign,
                prevNextButtons: showButtons,
                pageDots: showDots,
                wrapAround: wrapAround,
                draggable: draggable,
                adaptiveHeight: $carousel.data('adaptive-height'),
                freeScroll: $carousel.data('free-scroll'),
                autoPlay: autoPlay,
                groupCells: groupCells
            };

        $carousel.flickity(flickityOptions);

        if ($carousel.data('destroy-slider') === 1) {
            $carousel.flickity('destroy');
            $carousel.addClass('flickity-destroyed');
            flickityDefault = 'disabled';
        }

        $(window).resize(function () {
            flickySliderInit(getCurrentScreenSizeClass(), $carousel, flickityOptions, flickityDefault);
        });

        flickySliderInit(getCurrentScreenSizeClass(), $carousel, flickityOptions, flickityDefault);
    });

    /**
     * @returns {string}
     */
    function getCurrentScreenSizeClass() {
        var current_screen_size = $(window).width();
        var screen_size_class = 'lg';

        if (current_screen_size < 768) {
            screen_size_class = 'xs';
        }
        if (current_screen_size >= 768 && current_screen_size < 992) {
            screen_size_class = 'sm';
        }
        if (current_screen_size >= 992 && current_screen_size < 1200) {
            screen_size_class = 'md';
        }
        if (current_screen_size >= 1200) {
            screen_size_class = 'lg';
        }

        return screen_size_class;
    }

    function flickySliderInit(screen_size, carousel, flickityOptions, flickityDefault) {
        if (carousel.hasClass(screen_size + '_destroy') && !carousel.hasClass('flickity-destroyed')) {
            carousel.flickity('destroy');
            carousel.addClass('flickity-destroyed');
            return;
        }
        else {
            if (carousel.hasClass(screen_size + '_enabled') && carousel.hasClass('flickity-destroyed')) {
                carousel.removeClass('flickity-destroyed');
                carousel.flickity(flickityOptions);
                carousel.flickity('reloadCells');
                carousel.flickity('resize');
                return;
            }
        }

        if (!carousel.hasClass(screen_size + '_enabled') && !carousel.hasClass(screen_size + '_destroy')) {
            if (flickityDefault == 'enabled' && carousel.hasClass('flickity-destroyed')) {
                carousel.removeClass('flickity-destroyed');
                carousel.flickity(flickityOptions);
                carousel.flickity('reloadCells');
                carousel.flickity('resize');
                return;
            }
            if (flickityDefault == 'disabled' && !carousel.hasClass('flickity-destroyed')) {
                carousel.flickity('destroy');
                carousel.addClass('flickity-destroyed');
                return;
            }
        }
    }

    return flickySlider;

})
(jQuery);

