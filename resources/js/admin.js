jQuery(document).ready(function ($) {

    var checked_slider_source = $('input[name="source"]:checked').val();
    if (checked_slider_source) {
        showSourceOptions(checked_slider_source);
    }

    $('input[name="source"]').change(function () {
        if ($(this).is(':checked')) {
            showSourceOptions($(this).val());
        }
    });

    $('#select_post_types').on('change', function () {
        var nonce = $(this).data('nonce');
        var element = $(this);
        var selected = [];

        $("option:selected", element).each(function () {
            selected.push($(this).val());
        });

        $.ajax({
            type: "POST",
            dataType: "html",
            url: mhAjax.ajaxurl,
            data: {
                action: 'mh_flicky_slider_post_type_change',
                post_type_slugs: selected,
                nonce: nonce
            }
        }).done(function (html) {
            $('.el_post_categories > td').html(html);
        });
    });

    var $sortableList = $('#sortable_container > #sortable');
    $sortableList.sortable({
        cursor: 'move'
    });
    $sortableList.on('sortstop', function (event, ui) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: mhAjax.ajaxurl,
            data: {
                action: 'mh_flicky_slider_post_orders_change',
                nonce: $sortableList.data('nonce'),
                post_id: $sortableList.data('post-id'),
                post_orders: $sortableList.sortable("toArray")
            }
        }).done(function (data) {
            if (data.type === 'success') {

            }
        });
    });

    $('#changeSortOrderForPost').on('change', function () {
        var $selectElement = $(this);

        $.ajax({
            type: "POST",
            dataType: "json",
            url: mhAjax.ajaxurl,
            data: {
                action: 'mh_flicky_slider_post_sort_order_change',
                nonce: $selectElement.data('nonce'),
                post_id: $selectElement.data('post-id'),
                sort_by: $selectElement.val()
            }
        }).done(function (data) {
            if (data.type === 'success') {

            }
        });
    });

    /**
     *
     * @param value
     */
    function showSourceOptions(value) {
        if (value === 'post_ids') {
            $('.el_post_types, .el_post_categories').hide();
            $('.el_post_ids').show();
        }
        else if (value === 'post') {
            $('.el_post_types, .el_post_categories').show();
            $('.el_post_ids').hide();
        }
    }

});