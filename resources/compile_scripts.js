var fs = require("fs");
var browserify = require("browserify");

browserify(["./resources/js/frontend.js"])
    .plugin('tinyify', {flat: false})
    .transform("babelify", {presets: ["@babel/preset-env"]})
    .bundle()
    .pipe(fs.createWriteStream("./assets/js/frontend.min.js"));

browserify("./resources/js/admin.js")
    .plugin('tinyify', {flat: false})
    .transform("babelify", {presets: ["@babel/preset-env"]})
    .bundle()
    .pipe(fs.createWriteStream("./assets/js/admin.min.js"));