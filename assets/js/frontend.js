!function () {
    return function e(t, a, i) {
        function r(l, s) {
            if (!a[l]) {
                if (!t[l]) {
                    var o = "function" == typeof require && require;
                    if (!s && o) return o(l, !0);
                    if (d) return d(l, !0);
                    var n = new Error("Cannot find module '" + l + "'");
                    throw n.code = "MODULE_NOT_FOUND", n
                }
                var f = a[l] = {exports: {}};
                t[l][0].call(f.exports, function (e) {
                    return r(t[l][1][e] || e)
                }, f, f.exports, e, t, a, i)
            }
            return a[l].exports
        }

        for (var d = "function" == typeof require && require, l = 0; l < i.length; l++) r(i[l]);
        return r
    }
}()({
    1: [function (e, t, a) {
        "use strict";

        function i(e, t, a) {
            return t in e ? Object.defineProperty(e, t, {
                value: a,
                enumerable: !0,
                configurable: !0,
                writable: !0
            }) : e[t] = a, e
        }

        var r = function (e) {
            function t() {
                var t = e(window).width(), a = "lg";
                return t < 768 && (a = "xs"), t >= 768 && t < 992 && (a = "sm"), t >= 992 && t < 1200 && (a = "md"), t >= 1200 && (a = "lg"), a
            }

            function a(e, t, a, i) {
                if (t.hasClass(e + "_destroy") && !t.hasClass("flickity-destroyed")) return t.flickity("destroy"), void t.addClass("flickity-destroyed");
                if (t.hasClass(e + "_enabled") && t.hasClass("flickity-destroyed")) return t.removeClass("flickity-destroyed"), t.flickity(a), t.flickity("reloadCells"), void t.flickity("resize");
                if (!t.hasClass(e + "_enabled") && !t.hasClass(e + "_destroy")) {
                    if ("enabled" == i && t.hasClass("flickity-destroyed")) return t.removeClass("flickity-destroyed"), t.flickity(a), t.flickity("reloadCells"), void t.flickity("resize");
                    if ("disabled" == i && !t.hasClass("flickity-destroyed")) return t.flickity("destroy"), void t.addClass("flickity-destroyed")
                }
            }

            return e(document).ready(function () {
                e(".flicky-slider").each(function () {
                    var r, d = e(this);
                    d.removeClass("is-hidden"), d[0].offsetHeight;
                    var l = !1;
                    1 === d.data("auto-play") && (l = !0);
                    var s = null, o = d.data("group-cells");
                    !0 === o ? s = !0 : parseInt(o) > 0 && (s = parseInt(o));
                    var n = void 0 === d.data("cell-align") ? "left" : d.data("cell-align"),
                        f = void 0 === d.data("buttons") || d.data("buttons"),
                        u = void 0 === d.data("dots") || d.data("dots"),
                        c = void 0 === d.data("wrap-around") || d.data("wrap-around"),
                        y = void 0 === d.data("draggable") || d.data("draggable"), v = "enabled",
                        g = (i(r = {lazyLoad: !0}, "lazyLoad", 4), i(r, "imagesLoaded", !0), i(r, "contain", !0), i(r, "cellAlign", n), i(r, "prevNextButtons", f), i(r, "pageDots", u), i(r, "wrapAround", c), i(r, "draggable", y), i(r, "adaptiveHeight", d.data("adaptive-height")), i(r, "freeScroll", d.data("free-scroll")), i(r, "autoPlay", l), i(r, "groupCells", s), r);
                    d.flickity(g), 1 === d.data("destroy-slider") && (d.flickity("destroy"), d.addClass("flickity-destroyed"), v = "disabled"), e(window).resize(function () {
                        a(t(), d, g, v)
                    }), a(t(), d, g, v)
                })
            }), r
        }(jQuery)
    }, {}]
}, {}, [1]);
