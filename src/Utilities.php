<?php

namespace Musterhaus\FlickySliderWP;

/**
 * Class Utilities
 * @package Musterhaus\FlickySliderWP
 */
class Utilities
{

    /**
     * @param \WP_Post $post
     * @return false|string
     */
    public static function getPermalink(\WP_Post $post)
    {
        $link_type = get_post_meta($post->ID, 'mh_flickity_link_type', true);

        if ($link_type != 'no_link') {
            $custom_url = get_post_meta($post->ID, 'mh_flickity_custom_url', true);
            if (filter_var($custom_url, FILTER_VALIDATE_URL)) {
                return esc_url($custom_url);
            }

            return get_permalink($post);
        }

        return 'javascript:void();';
    }

    /**
     * @param $post
     * @param int $limit
     * @param string $delimiter
     * @param string $limit_type
     * @return mixed|string
     */
    public static function getExcerpt(\WP_Post $post, $limit = 55, $delimiter = ' ', $limit_type = 'words')
    {
        $excerpt = $post->post_excerpt;

        if (function_exists("qtrans_split")) {
            $arrExcerpt = qtrans_split($excerpt);
            $lang = get_bloginfo("language");
            if ($lang) {
                $arrLang = explode("-", $lang);
                $lang = $arrLang[0];
                $lang = strtolower($lang);
            }

            if (isset($arrExcerpt[$lang]))
                $excerpt = $arrExcerpt[$lang];
        }

        $excerpt = trim($excerpt);

        if (empty($excerpt))
            $excerpt = $post->post_content;

        $excerpt = strip_tags($excerpt);

        if ($limit_type == 'words')
            $excerpt = self::getTextIntro($excerpt, $limit, $delimiter);
        else
            $excerpt = self::getTextIntroChar($excerpt, $limit);

        return $excerpt;
    }

    /**
     * @param $post
     * @param int $limit
     * @param string $delimiter
     * @param string $limit_type
     * @return mixed|string
     */
    public static function getTitle(\WP_Post $post, $limit = 14, $delimiter = ' ', $limit_type = 'words')
    {
        $title = $post->post_title;
        if ($limit_type == 'words') {
            $title = self::getTextIntro($title, $limit, $delimiter);
        } else {
            $title = self::getTextIntroChar($title, $limit);
        }

        return $title;
    }

    /**
     * get text intro, limit by number of words
     *
     * @param $text
     * @param $limit
     * @param string $delimiter
     * @return mixed
     */
    public static function getTextIntro($text, $limit, $delimiter = ' ')
    {
        $delimiter = ' '; //function not yet available
        //if($delimiter == '') $delimiter = ' ';
        $limit++;
        $arrIntro = explode($delimiter, $text, $limit);

        if (count($arrIntro) >= $limit) {
            array_pop($arrIntro);
            $intro = implode($delimiter, $arrIntro);
            $intro = trim($intro);
            if (!empty($intro))
                $intro .= '...';
        } else {
            $intro = implode($delimiter, $arrIntro);
        }

        $intro = preg_replace('`\[[^\]]*\]`', '', $intro);
        return ($intro);
    }

    /**
     * get text intro, limit by number of chars
     *
     * @param $text
     * @param $limit
     * @return string
     */
    public static function getTextIntroChar($text, $limit)
    {
        $text = mb_substr($text, 0, $limit);
        return $text;
    }

}