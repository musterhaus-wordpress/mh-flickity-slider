<?php

namespace Musterhaus\FlickySliderWP;

/**
 * Class Slider_Table
 * @package Musterhaus\FlickySliderWP
 */
class Slider_Table extends \WP_List_Table
{

    /**
     * Slider_Table constructor.
     * @param array $args
     */
    public function __construct($args = array())
    {
        parent::__construct([
            'ajax' => false,
        ]);
    }

    /**
     * @param string $which
     */
    public function extra_tablenav($which)
    {
        if ($which == "top") {
        }
        if ($which == "bottom") {
        }
    }

    /**
     * @return array
     */
    public function get_columns()
    {
        return $columns = [
            'ID' => __('ID'),
            'post_name' => __('Alias'),
            'post_title' => __('Name'),
            'shortcode' => __('Shortcode'),
            'actions' => __('Actions'),
        ];
    }

    /**
     * @return array
     */
    public function get_sortable_columns()
    {
        return $sortable = array(
            'ID' => ['ID', false],
            'post_name' => ['post_name', false],
            'post_title' => ['post_title', false],
        );
    }

    /**
     * @param object $item
     * @param string $column_name
     * @return mixed
     */
    public function column_default($item, $column_name)
    {
        if ($column_name === 'ID') {
            return '<a href="' . menu_page_url('mh-flickity-slider', false) . '&view=edit&id=' . $item->$column_name . '">' . $item->$column_name . '</a>';
        }
        if ($column_name === 'actions') {
            $html = '<a class="button button-primary" href="' . menu_page_url('mh-flickity-slider', false) . '&view=edit&id=' . $item->ID . '">Bearbeiten</a>';
            $html .= ' <a class="button button-secondary" href="' . menu_page_url('mh-flickity-slider', false) . '&view=slides&id=' . $item->ID . '">Slides bearbeiten</a>';
            return $html;
        }
        if ($column_name === 'shortcode') {
            return '<code>[mh_flicky_slider alias="' . $item->post_name . '"]</code>';
        }

        if (isset($item->$column_name)) {
            return $item->$column_name;
        }

        return "";
    }

    /**
     * Prepare the table with different parameters, pagination, columns and table elements
     */
    public function prepare_items()
    {
        global $wpdb;

        $orderby = filter_input(INPUT_GET, 'orderby', FILTER_SANITIZE_STRING, ['flags' => FILTER_NULL_ON_FAILURE]);
        $order = filter_input(INPUT_GET, 'order', FILTER_SANITIZE_STRING, ['flags' => FILTER_NULL_ON_FAILURE]);

        $total_items = $wpdb->get_col('SELECT COUNT(ID) FROM wp_posts WHERE post_type = "mh_flickity_slider"');

        $screen = get_current_screen();
        $per_page = 20;
        $current_page = $this->get_pagenum();
        $total_items = $total_items[0];

        $columns = $this->get_columns();
        $sortable = $this->get_sortable_columns();
        $hidden = [];
        $this->_column_headers = array($columns, $hidden, $sortable);

        $query = new \WP_Query([
            'post_type' => 'mh_flickity_slider',
            'posts_per_page' => $per_page,
            'paged' => $current_page,
            'orderby' => $orderby,
            'order' => $order,
        ]);

        $this->set_pagination_args([
            'total_items' => $total_items,
            'per_page' => $per_page,
        ]);

        $this->items = $query->get_posts();;
    }

}