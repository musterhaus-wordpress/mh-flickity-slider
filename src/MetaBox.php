<?php
/**
 * Created by PhpStorm.
 * User: Sebastian Okolowski
 * Date: 02.02.2017
 * Time: 12:29
 */

namespace Musterhaus\FlickySliderWP;

/**
 * Class MetaBox
 * @package Musterhaus\FlickySliderWP
 */
class MetaBox
{

    const SAVE_POST_NONCE = 'mh_flickity_meta_box_nonce';

    protected $supported_post_types;

    /**
     * MetaBox constructor.
     * @param array $supported_post_types
     */
    public function __construct(array $supported_post_types = array())
    {
        $this->supported_post_types = $supported_post_types;

        add_action('load-post.php', [$this, 'setup']);
        add_action('load-post-new.php', [$this, 'setup']);
    }

    /**
     *
     */
    public function setup()
    {
        add_action('add_meta_boxes', function () {
            add_meta_box(
                'mh-flickity-slider',
                esc_html('MH Flickity Slider Settings'),
                [$this, 'metaBox'],
                $this->supported_post_types,
                'side',
                'default'
            );
        });

        add_action('save_post', [$this, 'savePost'], 10, 2);
    }

    /**
     * @param $object
     * @param $box
     */
    public function metaBox($object, $box)
    {
        wp_nonce_field(basename(__FILE__), self::SAVE_POST_NONCE);
        $meta_key = 'mh_flickity_link_type';
        $link_type = get_post_meta($object->ID, $meta_key, true);
        $options = [
            'no_link' => 'Kein Link',
            'default' => 'Standard',
            'custom' => 'Custom URL'
        ];
        ?>
        <p>
            <label>
                Link
                <select name="<?php echo $meta_key ?>" class="widefat">
                    <?php foreach ($options as $value => $label): ?>
                        <option value="<?php echo $value ?>" <?php echo ($link_type === $value) ? 'selected="selected"' : ''?>>
                            <?php echo $label ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </label>
        </p>
        <?php
        $meta_key = 'mh_flickity_custom_url';
        $custom_url = get_post_meta($object->ID, $meta_key, true);
        ?>
        <p>
            <label>
                Custom URL
                <input type="text" name="<?php echo $meta_key ?>" class="widefat"
                       value="<?php echo esc_url($custom_url) ?>">
            </label>
        </p>
        <?php
    }

    /**
     * @param $post_id
     * @param $post
     * @return mixed
     */
    public function savePost($post_id, $post)
    {
        if (!isset($_POST[self::SAVE_POST_NONCE]) || !wp_verify_nonce($_POST[self::SAVE_POST_NONCE], basename(__FILE__))) {
            return $post_id;
        }

        $post_type = get_post_type_object($post->post_type);
        if (!current_user_can($post_type->cap->edit_post, $post_id)) {
            return $post_id;
        }

        $custom_url = filter_input(INPUT_POST, 'mh_flickity_custom_url', FILTER_VALIDATE_URL, ['flags' => FILTER_NULL_ON_FAILURE]);
        if ($custom_url != null) {
            update_post_meta($post_id, 'mh_flickity_custom_url', $custom_url);
        } else {
            delete_post_meta($post_id, 'mh_flickity_custom_url');
        }

        $link_type = filter_input(INPUT_POST, 'mh_flickity_link_type', FILTER_SANITIZE_STRING, ['flags' => FILTER_NULL_ON_FAILURE]);
        if ($link_type != null) {
            update_post_meta($post_id, 'mh_flickity_link_type', $link_type);
        }
    }

}