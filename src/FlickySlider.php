<?php

namespace Musterhaus\FlickySliderWP;

/**
 * Class FlickySlider
 *
 * @package Musterhaus\FlickySliderWP
 */
class FlickySlider
{

    const POST_TYPE_SLUG = 'mh_flickity_slider';
    const AJAX_POST_TYPE_CHANGE_NONCE = 'mh_flicky_slider_post_type_change_nonce';
    const AJAX_POST_ORDERS_CHANGE_NONCE = 'mh_flicky_slider_post_orders_change_nonce';
    const AJAX_POST_SORT_ORDER_CHANGE_NONCE = 'mh_flicky_slider_post_sort_order_change_nonce';

    /**
     * Default Options for Slider
     *
     * @var array
     */
    protected $defaults = [
        'id' => 0,
        'title' => null,
        'alias' => null,
        'shortcode' => '',
        'source' => 'post',
        'post_ids' => null,
        'post_types' => null,
        'post_categories' => null,
        'sort_by' => 'ID',
        'sort_order' => 'ASC',
        'limit' => null,
        'limit_title' => 14,
        'limit_excerpt' => 42,
        'limit_by' => 'words',
        'template' => 'default',
        'image_ratio' => '16-9',
        'sliderDestroy' => 0,
        'prevNextButtons' => 1,
        'prevNextButtonsPosition' => 'bottom',
        'pageDots' => 1,
        'wrapAround' => 1,
        'draggable' => 1,
        'freeScroll' => 1,
        'cellAlign' => 'left',
        'groupCells' => false,
        'autoPlay' => 1,
        'adaptiveHeight' => 1,
        'dragThreshold' => 3,
        'selectedAttraction' => 0.025,
        'friction' => 0.28,
    ];

    /**
     * Default Options for Shortcode
     *
     * @var array
     */
    protected $shortcodeDefaults = [
        'id' => null,
        'alias' => null,
        'template' => null,
        'page_dots' => null,
        'prev_next_buttons' => null,
        'prev_next_buttons_position' => null,
        'slider_destroy' => null,
        'auto_play' => null,
        'wrap_around' => null,
    ];

    /**
     * Slider Edit Validation Filter Rules
     *
     * @var array
     */
    protected $sliderValidationRules = [
        '_wpnonce' => FILTER_SANITIZE_STRING,
        'id' => FILTER_VALIDATE_INT,
        'title' => FILTER_SANITIZE_STRING,
        'alias' => FILTER_SANITIZE_STRING,
        'source' => FILTER_SANITIZE_STRING,
        'post_ids' => FILTER_SANITIZE_STRING,
        'post_types' => [
            'filter' => FILTER_SANITIZE_STRING,
            'flags' => FILTER_REQUIRE_ARRAY,
        ],
        'post_categories' => [
            'filter' => FILTER_SANITIZE_STRING,
            'flags' => FILTER_REQUIRE_ARRAY,
        ],
        'sort_by' => FILTER_SANITIZE_STRING,
        'sort_order' => FILTER_SANITIZE_STRING,
        'limit' => FILTER_SANITIZE_NUMBER_INT,
        'limit_title' => FILTER_VALIDATE_INT,
        'limit_excerpt' => FILTER_VALIDATE_INT,
        'limit_by' => FILTER_SANITIZE_STRING,
        'template' => FILTER_SANITIZE_STRING,
        'image_ratio' => FILTER_SANITIZE_STRING,
        'sliderDestroy' => FILTER_VALIDATE_INT,
        'prevNextButtons' => FILTER_SANITIZE_STRING,
        'prevNextButtonsPosition' => FILTER_SANITIZE_STRING,
        'pageDots' => FILTER_SANITIZE_STRING,
        'wrapAround' => FILTER_SANITIZE_STRING,
        'draggable' => FILTER_SANITIZE_STRING,
        'freeScroll' => FILTER_SANITIZE_STRING,
        'cellAlign' => FILTER_SANITIZE_STRING,
        'groupCells' => FILTER_SANITIZE_STRING,
        'autoPlay' => FILTER_SANITIZE_STRING,
        'adaptiveHeight' => FILTER_SANITIZE_STRING,
        'dragThreshold' => FILTER_SANITIZE_STRING,
        'selectedAttraction' => FILTER_SANITIZE_STRING,
        'friction' => FILTER_SANITIZE_STRING,
    ];

    /**
     * FlickySlider constructor.
     */
    public function __construct()
    {
        add_action('wp_enqueue_scripts', function () {
            wp_enqueue_script('flickity', MH_FLICKY_SLIDER_URL . '/assets/js/flickity.pkgd.min.js', ['jquery'], false, true);
            wp_enqueue_script('flickity-frontend', MH_FLICKY_SLIDER_URL . '/assets/js/frontend.min.js', ['flickity'], false, true);
            wp_enqueue_style('flickity', MH_FLICKY_SLIDER_URL . '/assets/css/flickity.min.css');
            wp_enqueue_style('flickity-frontend', MH_FLICKY_SLIDER_URL . '/assets/css/frontend.min.css');
        });
        add_action('init', [$this, 'registerPostType']);
        add_action('init', [$this, 'registerShortcode']);
        add_action('admin_enqueue_scripts', [$this, 'enqueueAdminScripts']);
        add_action('admin_menu', [$this, 'adminMenu']);
        add_action('admin_init', [$this, 'initSettingSectionsAndFields']);
        add_action('wp_ajax_mh_flicky_slider_post_type_change', [$this, 'ajaxCallbackPostTypes']);
        add_action('wp_ajax_mh_flicky_slider_post_orders_change', [$this, 'ajaxCallbackPostOrders']);
        add_action('wp_ajax_mh_flicky_slider_post_sort_order_change', [$this, 'ajaxCallbackPostSortOrder']);

        $supported_post_types = apply_filters('mh_flicky_supported_post_types', ['oxy_service', 'oxy_slideshow_image']);
        $metaBox = new MetaBox($supported_post_types);
    }

    /**
     *
     */
    public function registerPostType()
    {
        register_post_type(self::POST_TYPE_SLUG, [
            'labels' => [
                'name' => __('Flickity Sliders'),
                'singular_name' => __('Flickity Slider'),
            ],
            'capability_type' => 'page',
            'public' => false,
            'show_in_rest' => true,
            'has_archive' => false,
        ]);
    }

    /**
     *
     */
    public function registerShortcode()
    {
        add_shortcode('mh_flicky_slider', [$this, 'renderShortcodeContent']);
    }

    /**
     * @param      $atts
     * @param null $content
     *
     * @return string
     */
    public function renderShortcodeContent($atts, $content = null): string
    {
        $atts = shortcode_atts($this->shortcodeDefaults, $atts);
        $id = filter_var($atts['id'], FILTER_VALIDATE_INT);
        $alias = filter_var($atts['alias'], FILTER_SANITIZE_STRING);

        if ($alias) {
            $id = $this->getPostIdByAlias($alias);
        }

        if ($id) {
            $post = get_post($id);
            if (!$post instanceof \WP_Post) {
                return '';
            }

            if (get_post_type($post) !== self::POST_TYPE_SLUG) {
                return '';
            }

            $content = json_decode($post->post_content, true);
            $posts = $this->getPostsForSliderBySource($content['source'], $content);

            if (count($posts)) {
                $new_atts = [];
                array_walk($atts, function ($item, $key) use (&$new_atts) {
                    $key = lcfirst(implode('', array_map('ucfirst', explode('_', $key))));
                    if (!is_null($item)) {
                        $new_atts[$key] = $item;
                    }
                });

                $content = array_merge($content, $new_atts);
                $get_data_attributes = function () use ($content) {
                    return 'data-buttons="' . (bool)$content['prevNextButtons'] . '" data-dots="' . (bool)$content['pageDots'] .
                        '" data-cell-align="' . $content['cellAlign'] . '" data-destroy-slider="' . (bool)$content['sliderDestroy'] .
                        '" data-wrap-around="' . (bool)$content['wrapAround'] . '" data-draggable="' . (bool)$content['draggable'] .
                        '" data-free-scroll="' . (bool)$content['freeScroll'] . '" data-group-cells="' . $content['groupCells'] .
                        '" data-auto-play="' . (bool)$content['autoPlay'] . '" data-adaptive-height="' . (bool)$content['adaptiveHeight'] .
                        '" data-drag-threshold="' . $content['dragThreshold'] . '" data-selected-attraction="' . $content['selectedAttraction'] .
                        '" data-friction="' . $content['friction'] . '"';
                };
                $get_nav_position = function () use ($content) {
                    return 'nav-' . $content['prevNextButtonsPosition'];
                };

                $template = isset($content['template']) ? $content['template'] : 'default.phtml';
                $template_paths = apply_filters('mh_flicky_template_dirs', [MH_FLICKY_SLIDER_DIR . '/templates']);

                if (isset($atts['template'])) {
                    foreach ($template_paths as $path) {
                        if (file_exists($path . DIRECTORY_SEPARATOR . $atts['template'])) {
                            $template = $atts['template'];
                            continue;
                        }
                    }
                }

                $html = '<div class="flicky-silder-container ' . $get_nav_position() . '" id="mh_flicky_slider_' . $post->ID . '" data-id="' . $post->ID . '" data-alias="' . $post->post_name . '">';
                ob_start();
                require MH_FLICKY_SLIDER_DIR . '/templates/' . $template;
                $html .= ob_get_clean();
                $html .= '</div>';
                wp_reset_postdata();

                return $html;
            }
        }

        return '';
    }

    public function getPostIdByAlias($alias)
    {
        global $wpdb;
        $postID = $wpdb->get_var($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type = %s", $alias, self::POST_TYPE_SLUG));
        if ($postID) {
            return (int)$postID;
        }

        return null;
    }

    /**
     * @param $source
     * @param $content
     *
     * @return array
     */
    private function getPostsForSliderBySource($source, $content)
    {
        $query_filter = [];

        $post_orders = get_post_meta($content['id'], 'post_orders', true);
        if (is_array($post_orders) && count($post_orders) && $content['sort_by'] === 'menu_order') {
            $source = 'custom_order';
        }

        switch ($source) {
            case 'custom_order':
                $query_filter = $this->getPostIdsQueryFilter($post_orders, $content);
                break;
            // Nach Post IDs
            case 'post_ids':
                $post_ids = explode(',', (string)$content['post_ids']);
                $query_filter = $this->getPostIdsQueryFilter($post_ids);
                break;
            // Nach Posts in Kategorien
            case 'post':
                $query_filter = $this->getPostQueryFilter($content);
                break;
        }

        $query = new \WP_Query($query_filter);
        $posts = $query->get_posts();
        $query->reset_postdata();

        return $posts;
    }

    /**
     * @param array $post_ids
     * @param       $content
     *
     * @return array
     */
    private function getPostIdsQueryFilter(array $post_ids, $content)
    {
        $orderby = $content['sort_by'];
        if ($orderby === 'menu_order') {
            $orderby = 'post__in';
        }

        $query_filter = [
            'post__in' => $post_ids,
            'post_status' => 'publish',
            'orderby' => $orderby,
            'order' => $content['sort_order'],
            'posts_per_page' => $content['limit'],
            'ignore_sticky_posts' => 1,
        ];

        return $query_filter;
    }

    /**
     * @param $content
     *
     * @return arrays
     */
    private function getPostQueryFilter($content)
    {
        $tax_query = null;
        $taxonomies = [];

        if (isset($content['post_categories']) && count($content['post_categories'])) {
            $post_categories = $content['post_categories'];
            foreach ($post_categories as $post_category) {
                $exploded_category_tax_and_id = explode('#', $post_category);
                $taxonomies[$exploded_category_tax_and_id[0]][] = $exploded_category_tax_and_id[1];
            }

            if (count($taxonomies)) {
                $tax_query = [
                    'relation' => 'OR'
                ];

                foreach ($taxonomies as $tax => $ids) {
                    $tax_query[] = [
                        'taxonomy' => $tax,
                        'field' => 'term_id',
                        'terms' => $ids,
                        'operator' => 'IN',
                    ];
                };
            }
        }

        $query_filter = [
            'post_type' => $content['post_types'],
            'post_status' => 'publish',
            'orderby' => $content['sort_by'],
            'order' => $content['sort_order'],
            'tax_query' => $tax_query,
            'ignore_sticky_posts' => 1
        ];

        if ($content['limit']) {
            $query_filter['posts_per_page'] = $content['limit'];
        } else {
            $query_filter['nopaging'] = true;
        }

        return $query_filter;
    }

    /**
     * @param $hook
     */
    public function enqueueAdminScripts($hook)
    {
        if ($hook != 'toplevel_page_mh-flickity-slider') {
            return;
        }

        wp_enqueue_style('mh_flickity', MH_FLICKY_SLIDER_URL . '/assets/css/admin.min.css');
        wp_register_script('mh_flickity', MH_FLICKY_SLIDER_URL . '/assets/js/admin.min.js', ['jquery', 'jquery-ui-sortable']);
        wp_localize_script('mh_flickity', 'mhAjax', ['ajaxurl' => admin_url('admin-ajax.php')]);
        wp_enqueue_script('mh_flickity');
    }

    /**
     *
     */
    public function initSettingSectionsAndFields()
    {
        register_setting(self::POST_TYPE_SLUG, self::POST_TYPE_SLUG . '_settings');

        foreach ($this->getEditSections() as $section) {
            add_settings_section($section['id'], $section['title'], $section['callback'], self::POST_TYPE_SLUG);
        }
        foreach ($this->getEditSectionFields() as $section => $fields) {
            foreach ($fields as $field) {
                add_settings_field($field['id'], $field['title'], $field['callback'], self::POST_TYPE_SLUG, $section, ['class' => $field['class']]);
            }
        }
    }

    /**
     * Lade die Kategorien und Schlagworte nach übergebenen Post Types
     */
    public function ajaxCallbackPostTypes()
    {
        if (!wp_verify_nonce($_REQUEST['nonce'], self::AJAX_POST_TYPE_CHANGE_NONCE)) {
            wp_die(__('Invalid nonce given.'));
        }

        $category_groups = $this->getCategoryGroupsForPostTypes($_REQUEST['post_type_slugs']);
        ?>

        <select multiple="multiple" name="post_categories[]"
                size="10" id="select_post_categories" class="widefat">
            <?php foreach ($category_groups as $group_name => $category_group): ?>
                <optgroup label="<?php echo $group_name ?>">
                    <?php foreach ($category_group as $category): ?>
                        <option value="<?php echo $category['slug_id'] ?>"><?php echo $category['name'] ?></option>
                    <?php endforeach; ?>
                </optgroup>
            <?php endforeach; ?>
        </select>

        <?php
        die();
    }

    /**
     * Speichere die Reihenfolge der Posts per Drag&Drop Sortable
     */
    public function ajaxCallbackPostOrders()
    {
        if (!wp_verify_nonce($_REQUEST['nonce'], self::AJAX_POST_ORDERS_CHANGE_NONCE)) {
            wp_die(__('Invalid nonce given.'));
        }

        $validated_post_array = filter_input_array(INPUT_POST, [
            'post_id' => FILTER_VALIDATE_INT,
            'post_orders' => [
                'filter' => FILTER_SANITIZE_STRING,
                'flags' => FILTER_REQUIRE_ARRAY,
            ],
        ]);

        if ($validated_post_array) {
            $post = get_post($validated_post_array['post_id']);
            if ($post instanceof \WP_Post) {
                $updated = update_post_meta($post->ID, 'post_orders', $validated_post_array['post_orders']);
                if ($updated == true) {
                    $post_is_updated = $this->updatePostSortByOption($post->ID, 'menu_order');

                    echo json_encode(['type' => 'success', 'message' => 'Reihenfolge erfolgreich gespeichert.']);
                    die();
                }
            }
        }

        echo json_encode(['type' => 'error', 'message' => 'Neue Reihenfolge konnte nicht gespeichert werden.']);
        die();
    }

    /**
     *
     */
    public function ajaxCallbackPostSortOrder()
    {
        if (!wp_verify_nonce($_REQUEST['nonce'], self::AJAX_POST_SORT_ORDER_CHANGE_NONCE)) {
            wp_die(__('Invalid nonce given.'));
        }

        $validated_post_array = filter_input_array(INPUT_POST, [
            'post_id' => FILTER_VALIDATE_INT,
            'sort_by' => [
                'filter' => FILTER_SANITIZE_STRING,
            ],
        ]);

        if ($validated_post_array) {
            $post_is_updated = $this->updatePostSortByOption($validated_post_array['post_id'], $validated_post_array['sort_by']);
            if ($post_is_updated === true) {
                echo json_encode([
                    'type' => 'success',
                    'message' => 'Neue Sortierung gespeichert.'
                ]);

                die();
            }
        }

        echo json_encode(['type' => 'error', 'message' => 'Neue Sortierung konnte nicht gespeichert werden.']);
        die();
    }

    /**
     * @param $post_id
     * @param $sort_by
     *
     * @return bool
     */
    private function updatePostSortByOption($post_id, $sort_by): bool
    {
        $post = get_post($post_id);
        if ($post instanceof \WP_Post) {
            $post_content = json_decode($post->post_content, true);
            $post_content = array_merge($post_content, ['sort_by' => $sort_by]);

            $inserted_ID = wp_update_post([
                'ID' => $post->ID,
                'post_content' => json_encode($post_content),
            ]);

            if ($inserted_ID instanceof \WP_Error || $inserted_ID === 0) {
                return false;
            } else {
                return true;
            }
        }

        return false;
    }

    /**
     * @return void
     */
    public function adminMenu()
    {
        add_menu_page('MH Flickity Sliders', 'Flickity Sliders', 'manage_options', 'mh-flickity-slider', [$this, 'adminMenuPage'], MH_FLICKY_SLIDER_URL . '/assets/img/wp-menu-mh.png');
        /*add_submenu_page('mh-flickity-slider', 'Import', 'Import', 'manage_options', 'mh-flicky-slider-import', [$this, 'importFromShowbizSlider']);*/
    }

    /**
     * @return mixed|void
     */
    public function adminMenuPage()
    {
        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.'));
        }

        if ($filtered = filter_input_array(INPUT_POST, $this->sliderValidationRules)) {
            if (!wp_verify_nonce($filtered['_wpnonce'], 'mh_flickity_slider-options')) {
                return wp_safe_redirect(menu_page_url('mh-flickity-slider', false));
            }

            if (empty($filtered['id'])) {
                $inserted_ID = wp_insert_post([
                    'post_title' => $filtered['title'],
                    'post_name' => $filtered['alias'],
                    'post_content' => json_encode($filtered),
                    'post_status' => 'publish',
                    'post_type' => self::POST_TYPE_SLUG
                ]);
            } else {
                $slider_ID = $filtered['id'];
                if (get_post_type($slider_ID) !== self::POST_TYPE_SLUG) {
                    return wp_safe_redirect(menu_page_url('mh-flickity-slider', false));
                }

                $inserted_ID = wp_update_post([
                    'ID' => $filtered['id'],
                    'post_title' => $filtered['title'],
                    'post_name' => $filtered['alias'],
                    'post_content' => json_encode($filtered),
                ]);
            }

            if ($inserted_ID instanceof \WP_Error || $inserted_ID === 0) {
                wp_die("Saving Error");
            }

            return wp_safe_redirect(menu_page_url('mh-flickity-slider', false) . '&view=edit&id=' . $inserted_ID);
        }

        if (filter_input(INPUT_GET, 'view') === 'edit') {
            $slider_ID = $this->getSliderIdByGetRequest();
            return require_once MH_FLICKY_SLIDER_DIR . '/views/edit.phtml';
        }

        if (filter_input(INPUT_GET, 'view') === 'slides') {
            $slider_ID = $this->getSliderIdByGetRequest();
            if (!$slider_ID) {
                wp_die(__('No Slider selected'));
            }

            $post = get_post($slider_ID);
            if (!$post instanceof \WP_Post) {
                wp_die(__('Could not find Post for given ID'));
            }

            $post_content = json_decode($post->post_content, true);
            $posts = $this->getPostsForSliderBySource($post_content['source'], $post_content);

            return require_once MH_FLICKY_SLIDER_DIR . '/views/slides.phtml';
        }

        $wp_list_table = new \Musterhaus\FlickySliderWP\Slider_Table();
        $wp_list_table->prepare_items();

        return require_once MH_FLICKY_SLIDER_DIR . '/views/sliders.phtml';
    }

    /**
     * @param $action
     *
     * @return string
     */
    public function getWPNonceForAction(string $action): string
    {
        return wp_create_nonce($action);
    }

    /**
     * @return mixed
     */
    private function getSliderIdByGetRequest()
    {
        $slider_ID = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
        if ($slider_ID && (get_post_type($slider_ID) !== self::POST_TYPE_SLUG)) {
            wp_die('Wrong Post.');
        }

        return $slider_ID;
    }

    /**
     * @return array
     */
    public function getEditSections()
    {
        $sections = [
            [
                'id' => 'mh_flickity_slider_section_main',
                'title' => __('Content'),
                'callback' => function () {
                },
            ],
            [
                'id' => 'mh_flickity_slider_section_secondary',
                'title' => __('Settings'),
                'callback' => function () {
                },
            ]
        ];

        return $sections;
    }

    /**
     * @return array
     */
    public function getEditSectionFields()
    {
        $radio_on_off = function ($name, $selected = null, $labels = ['On', 'Off']) {
            ?>
            <label>
                <input name="<?php echo $name ?>" type="radio"
                       value="1" <?php echo ($selected == 1) ? 'checked="checked"' : ''; ?>> <?php echo $labels[0] ?>
            </label>
            <label>
                <input name="<?php echo $name ?>" type="radio"
                       value="0" <?php echo ($selected == 0) ? 'checked="checked"' : ''; ?>> <?php echo $labels[1] ?>
            </label>
            <?php
        };

        $fields = [
            'mh_flickity_slider_section_main' => [
                [
                    'id' => 'title',
                    'title' => __('Slider Title'),
                    'class' => '',
                    'callback' => function () {
                        $post_content = $this->getCurrentPostContent();
                        echo '<input class="widefat" name="title" value="' . $post_content["title"] . '">';
                    },
                ],
                [
                    'id' => 'alias',
                    'title' => __('Slider Alias'),
                    'class' => '',
                    'callback' => function () {
                        $post_content = $this->getCurrentPostContent();
                        echo '<input class="widefat" name="alias" value="' . $post_content["alias"] . '">';
                    },
                ],
                [
                    'id' => 'shortcode',
                    'title' => __('Slider Shortcode'),
                    'class' => '',
                    'callback' => function () {
                        $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
                        $shortcode = '[mh_flicky_slider id=' . $id . ']';

                        echo '<input class="widefat" name="shortcode" readonly="readonly" value="' . $shortcode . '">';
                    },
                ],
                [
                    'id' => 'source',
                    'title' => __('Source'),
                    'class' => '',
                    'callback' => function () {
                        $post_content = $this->getCurrentPostContent();
                        $selected = $post_content['source'];
                        ?>
                        <label>
                            <input name="source" type="radio"
                                   value="post" <?php echo ($selected === 'post') ? 'checked="checked"' : ''; ?>>
                            Posts
                        </label>
                        <label>
                            <input name="source" type="radio"
                                   value="post_ids" <?php echo ($selected === 'post_ids') ? 'checked="checked"' : ''; ?>>
                            Post IDs
                        </label>
                        <?php
                    },
                ],
                [
                    'id' => 'post_ids',
                    'title' => __('Post IDs'),
                    'class' => 'el_post_ids',
                    'callback' => function () {
                        $post_content = $this->getCurrentPostContent();
                        ?>
                        <input class="widefat" id="el_post_ids" name="post_ids"
                               value="<?php echo $post_content['post_ids'] ?>">
                        <?php
                    },
                ],
                [
                    'id' => 'post_types',
                    'title' => __('Post Types'),
                    'class' => 'el_post_types',
                    'callback' => function () {
                        $post_content = $this->getCurrentPostContent();
                        $possible_post_types = $this->getPossiblePostTypes();
                        $selected_post_types = (array)$post_content['post_types'];
                        ?>
                        <select multiple="multiple" name="post_types[]"
                                size="10" id="select_post_types" class="widefat"
                                data-nonce="<?php echo wp_create_nonce(self::AJAX_POST_TYPE_CHANGE_NONCE) ?>">
                            <?php foreach ($possible_post_types as $possible_post_type):
                                $post_type = get_post_type_object($possible_post_type); ?>
                                <option value="<?php echo $possible_post_type ?>" <?php echo (in_array($possible_post_type, $selected_post_types)) ? 'selected="selected"' : ''; ?>><?php echo $post_type->label ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?php
                    },
                ],
                [
                    'id' => 'post_categories',
                    'title' => __('Post Categories'),
                    'class' => 'el_post_categories',
                    'callback' => function () {
                        $post_content = $this->getCurrentPostContent();
                        $selected_post_types = (array)$post_content['post_types'];
                        $category_groups = $this->getCategoryGroupsForPostTypes($selected_post_types);
                        $selected_post_categories = (array)$post_content['post_categories'];
                        ?>
                        <select multiple="multiple" name="post_categories[]"
                                size="10" id="select_post_categories" class="widefat">
                            <?php foreach ($category_groups as $group_name => $category_group): ?>
                                <optgroup label="<?php echo $group_name ?>">
                                    <?php foreach ($category_group as $category): ?>
                                        <option value="<?php echo $category['slug_id'] ?>" <?php echo (in_array($category['slug_id'], $selected_post_categories)) ? 'selected="selected"' : ''; ?>><?php echo $category['name'] ?></option>
                                    <?php endforeach; ?>
                                </optgroup>
                            <?php endforeach; ?>
                        </select>
                        <?php
                    },
                ],
                [
                    'id' => 'sort_by',
                    'title' => __('Sort by'),
                    'class' => '',
                    'callback' => function () {
                        $sliderID = $this->getPostIdByGetRequest();
                        $post_content = $this->getCurrentPostContent();
                        $selected_sort_by = $post_content['sort_by'];
                        ?>
                        <select name="sort_by">
                            <?php foreach ($this->getSortOrdersForPost() as $key => $name): ?>
                                <option value="<?php echo $key ?>" <?php echo ($selected_sort_by === $key) ? 'selected="selected"' : ''; ?>><?php echo $name ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?php if ($selected_sort_by === 'menu_order'):

                            ?>
                            <a class="button"
                               href="<?php echo menu_page_url('mh-flickity-slider', false) . '&view=slides&id=' . $sliderID ?>"><?php echo __('Edit Slides') ?></a>
                        <?php endif;
                    },
                ],
                [
                    'id' => 'sort_order',
                    'title' => __('Sort order'),
                    'class' => '',
                    'callback' => function () {
                        $post_content = $this->getCurrentPostContent();
                        $selected = $post_content['sort_order'];
                        ?>
                        <label>
                            <input name="sort_order" type="radio"
                                   value="asc" <?php echo ($selected === 'asc') ? 'checked="checked"' : ''; ?>> ASC
                        </label>
                        <label>
                            <input name="sort_order" type="radio"
                                   value="desc" <?php echo ($selected === 'desc') ? 'checked="checked"' : ''; ?>> DESC
                        </label>
                        <?php
                    },
                ],
                [
                    'id' => 'limit',
                    'title' => __('Limit visible Posts'),
                    'class' => 'el_limit',
                    'callback' => function () {
                        $post_content = $this->getCurrentPostContent();
                        ?>
                        <input class="widefat" id="" name="limit" value="<?php echo $post_content['limit'] ?>">
                        <?php
                    },
                ],
                [
                    'id' => 'limit_title',
                    'title' => __('Limit The Title To'),
                    'class' => 'el_limit_title',
                    'callback' => function () {
                        $post_content = $this->getCurrentPostContent();
                        ?>
                        <input class="widefat" id="" name="limit_title"
                               value="<?php echo $post_content['limit_title'] ?>">
                        <?php
                    },
                ],
                [
                    'id' => 'limit_excerpt',
                    'title' => __('Limit The Excerpt To'),
                    'class' => 'el_limit_excerpt',
                    'callback' => function () {
                        $post_content = $this->getCurrentPostContent();
                        ?>
                        <input class="widefat" id="" name="limit_excerpt"
                               value="<?php echo $post_content['limit_excerpt'] ?>">
                        <?php
                    },
                ],
                [
                    'id' => 'limit_by',
                    'title' => __('Limit By'),
                    'class' => 'el_limit_by',
                    'callback' => function () {
                        $post_content = $this->getCurrentPostContent();
                        $selected = $post_content['limit_by'];
                        $limit_by_options = [
                            'words' => 'Words',
                            'chars' => 'Characters',
                        ];
                        ?>
                        <select name="limit_by">
                            <?php foreach ($limit_by_options as $key => $name): ?>
                                <option value="<?php echo $key ?>" <?php echo ($selected === $key) ? 'selected="selected"' : ''; ?>><?php echo $name ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?php
                    },
                ],
                [
                    'id' => 'image_ratio',
                    'title' => __('Image Ratio'),
                    'class' => 'el_image_ratio',
                    'callback' => function () {
                        $post_content = $this->getCurrentPostContent();
                        $selected = $post_content['image_ratio'];
                        ?>
                        <select name="image_ratio">
                            <?php foreach ($this->getImageRatios() as $image_ratio): ?>
                                <option value="<?php echo $image_ratio['value'] ?>" <?php echo ($selected === $image_ratio['value']) ? 'selected="selected"' : ''; ?>><?php echo $image_ratio['label'] ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?php
                    },
                ],
                [
                    'id' => 'template',
                    'title' => __('Template'),
                    'class' => 'el_template',
                    'callback' => function () {
                        $post_content = $this->getCurrentPostContent();
                        $selected = $post_content['template'];
                        ?>
                        <select name="template">
                            <?php foreach ($this->getTemplates() as $key => $name): ?>
                                <option value="<?php echo $key ?>" <?php echo ($selected === $key) ? 'selected="selected"' : ''; ?>><?php echo $name ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?php
                    },
                ],
            ],
            'mh_flickity_slider_section_secondary' => [
                [
                    'id' => 'sliderDestroy',
                    'title' => __('Disabled Slider'),
                    'class' => '',
                    'callback' => function () use ($radio_on_off) {
                        $post_content = $this->getCurrentPostContent();
                        return $radio_on_off('sliderDestroy', $post_content['sliderDestroy'], [__('True'), __('False')]);
                    }
                ],
                [
                    'id' => 'prevNextButtons',
                    'title' => __('prevNextButtons'),
                    'class' => '',
                    'callback' => function () use ($radio_on_off) {
                        $post_content = $this->getCurrentPostContent();
                        return $radio_on_off('prevNextButtons', $post_content['prevNextButtons']);
                    }
                ],
                [
                    'id' => 'prevNextButtonsPosition',
                    'title' => __('Navigation Button Position'),
                    'class' => '',
                    'callback' => function () {
                        $post_content = $this->getCurrentPostContent();
                        $selected = $post_content['prevNextButtonsPosition'];
                        $options = [
                            ['value' => 'bottom', 'label' => 'Unter dem Slider'],
                            ['value' => 'top', 'label' => 'Über dem Slider'],
                            ['value' => 'left_right', 'label' => 'Links/Rechts im Slider'],
                        ];
                        ?>
                        <select name="prevNextButtonsPosition">
                            <?php foreach ($options as $option): ?>
                                <option value="<?php echo $option['value'] ?>" <?php echo ($selected === $option['value']) ? 'selected="selected"' : ''; ?>><?php echo $option['label'] ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?php
                    }
                ],
                [
                    'id' => 'pageDots',
                    'title' => __('pageDots'),
                    'class' => '',
                    'callback' => function () use ($radio_on_off) {
                        $post_content = $this->getCurrentPostContent();
                        return $radio_on_off('pageDots', $post_content['pageDots']);
                    }
                ],
                [
                    'id' => 'wrapAround',
                    'title' => __('wrapAround'),
                    'class' => '',
                    'callback' => function () use ($radio_on_off) {
                        $post_content = $this->getCurrentPostContent();
                        return $radio_on_off('wrapAround', $post_content['wrapAround']);
                    }
                ],
                [
                    'id' => 'draggable',
                    'title' => __('draggable'),
                    'class' => '',
                    'callback' => function () use ($radio_on_off) {
                        $post_content = $this->getCurrentPostContent();
                        return $radio_on_off('draggable', $post_content['draggable']);
                    }
                ],
                [
                    'id' => 'freeScroll',
                    'title' => __('freeScroll'),
                    'class' => '',
                    'callback' => function () use ($radio_on_off) {
                        $post_content = $this->getCurrentPostContent();
                        return $radio_on_off('freeScroll', $post_content['freeScroll']);
                    }
                ],
                [
                    'id' => 'cellAlign',
                    'title' => __('cellAlign'),
                    'class' => '',
                    'callback' => function () {
                        $post_content = $this->getCurrentPostContent();
                        $selected = $post_content['cellAlign'];
                        $aligns = ['left', 'center', 'right'];
                        ?>
                        <select name="cellAlign">
                            <?php foreach ($aligns as $align): ?>
                                <option value="<?php echo $align ?>" <?php echo ($selected === $align) ? 'selected="selected"' : ''; ?>><?php echo $align ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?php
                    },
                ],
                [
                    'id' => 'groupCells',
                    'title' => __('groupCells'),
                    'class' => '',
                    'callback' => function () {
                        $post_content = $this->getCurrentPostContent();
                        echo '<input class="widefat" name="groupCells" value="' . $post_content['groupCells'] . '">';
                    },
                ],
                [
                    'id' => 'autoPlay',
                    'title' => __('autoPlay'),
                    'class' => '',
                    'callback' => function () use ($radio_on_off) {
                        $post_content = $this->getCurrentPostContent();
                        return $radio_on_off('autoPlay', $post_content['autoPlay']);
                    }
                ],
                [
                    'id' => 'adaptiveHeight',
                    'title' => __('adaptiveHeight'),
                    'class' => '',
                    'callback' => function () use ($radio_on_off) {
                        $post_content = $this->getCurrentPostContent();
                        return $radio_on_off('adaptiveHeight', $post_content['adaptiveHeight']);
                    }
                ],
                [
                    'id' => 'dragThreshold',
                    'title' => __('dragThreshold'),
                    'class' => '',
                    'callback' => function () {
                        $post_content = $this->getCurrentPostContent();
                        echo '<input class="widefat" name="dragThreshold" value="' . $post_content['dragThreshold'] . '">';
                    },
                ],
                [
                    'id' => 'selectedAttraction',
                    'title' => __('selectedAttraction'),
                    'class' => '',
                    'callback' => function () {
                        $post_content = $this->getCurrentPostContent();
                        echo '<input class="widefat" name="selectedAttraction" value="' . $post_content['selectedAttraction'] . '">';
                    },
                ],
                [
                    'id' => 'friction',
                    'title' => __('friction'),
                    'class' => '',
                    'callback' => function () {
                        $post_content = $this->getCurrentPostContent();
                        echo '<input class="widefat" name="friction" value="' . $post_content['friction'] . '">';
                    },
                ]
            ],
        ];

        return $fields;
    }

    /**
     * @return array
     */
    public function getSortOrdersForPost(): array
    {
        return [
            'ID' => 'Post ID',
            'date' => 'Date',
            'title' => 'Title',
            'name' => 'Slug',
            'modified' => 'Last Modified',
            'author' => 'Author',
            'comment_count' => 'Number Of Comments',
            'rand' => 'Random',
            'none' => 'Unsorted',
            'menu_order' => 'Custom Order',
        ];
    }

    /**
     * @return array
     */
    public function getPossiblePostTypes(): array
    {
        //['public' => true]
        return get_post_types(['show_ui' => true]);
    }

    /**
     * @param $post_type_slug
     *
     * @return array
     */
    public function getCategoryGroupsForPostTypes($post_type_slug)
    {
        $category_groups = [];

        if (is_string($post_type_slug)) {
            $post_type_slug = [$post_type_slug];
        }

        foreach ($post_type_slug as $slug) {
            $taxonomies = get_object_taxonomies($slug, 'objects');
            foreach ($taxonomies as $taxonomy) {
                if ($taxonomy->show_ui === true) {
                    $categories = get_terms($taxonomy->name);
                    $group_name = $taxonomy->label . ' (' . $slug . ')';

                    foreach ($categories as $category) {
                        $slug_id = $category->taxonomy . '#' . $category->term_id;
                        $category_groups[$group_name][$category->term_id] = [
                            'id' => $category->term_id,
                            'slug_id' => $slug_id,
                            'name' => $category->name . ' (' . $category->count . ')'
                        ];
                    }
                }
            }
        }

        return $category_groups;
    }

    public function getImageRatios()
    {
        return [
            ['label' => '16:9', 'value' => '16-9'],
            ['label' => '16:10', 'value' => '16-10'],
            ['label' => '4:3', 'value' => '4-3'],
            ['label' => '3:4', 'value' => '3-4'],
        ];
    }

    /**
     * @return array
     */
    public function getTemplates(): array
    {
        $template_dir_paths = apply_filters('mh_flicky_template_dirs', [MH_FLICKY_SLIDER_DIR . '/templates']);
        $template_selection = [];
        foreach ($template_dir_paths as $template_dir_path) {
            if (is_dir($template_dir_path)) {
                $files = array_diff(scandir($template_dir_path), array('..', '.'));

                foreach ($files as $file) {
                    $template_selection[$file] = $file;
                }
            }
        }

        return $template_selection;
    }

    /**
     * @return mixed
     */
    public function getPostIdByGetRequest()
    {
        $post_id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
        return $post_id;
    }

    /**
     * @return array|null|\WP_Post
     */
    public function getCurrentPost()
    {
        $post = get_post($this->getPostIdByGetRequest());
        return $post;
    }

    /**
     * @return array
     */
    public function getCurrentPostContent(): array
    {
        $content = [];
        $post = $this->getCurrentPost();

        if ($post instanceof \WP_Post) {
            $content = json_decode($post->post_content, true);
        }

        return wp_parse_args($content, $this->defaults);
    }

    /**
     *
     */
    public function importFromShowbizSlider()
    {
        global $wpdb;
        $showbiz_sliders = $wpdb->get_results('SELECT * FROM wp_showbiz_sliders');
        foreach ($showbiz_sliders as $showbiz_slider) {
            $post_id = $this->getPostIdByAlias($showbiz_slider->alias);

            $content = json_decode($showbiz_slider->params, true);

            if ($content['source_type'] == 'posts') {
                $content['source'] = 'post';
            }

            $content['limit'] = $content['max_slider_posts'];
            $content['post_types'] = (array)$content['post_types'];
            $content['sort_order'] = strtolower($content['posts_sort_direction']);
            $content['sort_by'] = $content['post_sortby'];

            $content['post_categories'] = [];
            $current_categories = explode(',', $content['post_category']);
            $pattern = '/^(.*)_(\d+)$/';
            foreach ($current_categories as $current_category) {
                preg_match($pattern, $current_category, $matches);
                if (count($matches) == 3) {
                    $content['post_categories'][] = $matches[1] . '#' . $matches[2];
                }
            }

            unset($content['source_type'], $content['posts_sort_direction'], $content['post_sortby'], $content['max_slider_posts'], $content['post_category']);
            $new_content = json_encode($content);

            if (is_int($post_id)) {
                wp_update_post([
                    'ID' => $post_id,
                    'post_title' => $showbiz_slider->title,
                    'post_name' => $showbiz_slider->alias,
                    'post_content' => $new_content
                ]);

                echo "UPDATE: " . $post_id;
                echo "<hr>";
                continue;

            } else {
                $inserted_ID = wp_insert_post([
                    'post_title' => $showbiz_slider->title,
                    'post_name' => $showbiz_slider->alias,
                    'post_content' => $new_content,
                    'post_status' => 'draft',
                    'post_type' => self::POST_TYPE_SLUG
                ]);

                echo "INSERT: " . $inserted_ID;
                echo "<hr>";
                continue;
            }
        }
    }

    /**
     * @param string $imgsource
     * @param int    $width
     *
     * @return string
     */
    public static function getResizedImage(string $imgsource, int $width = 360): string
    {
        if (empty($imgsource)) {
            return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAJCAIAAAC0SDtlAAAAE0lEQVR4AWO4/eozSWhwaBjVAAA9uIeQdKcQNgAAAABJRU5ErkJggg==';
        }

        $pathinfo = pathinfo($imgsource);
        $imgUrlResized = $pathinfo['dirname'] . '/' . $pathinfo['filename'] . '-' . $width . '.' . $pathinfo['extension'];

        return $imgUrlResized;
    }

}
